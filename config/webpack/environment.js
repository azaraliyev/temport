const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

// environment.loaders.delete('sass')
// environment.loaders.delete('moduleSass')
// environment.loaders.delete('moduleCss')
// environment.loaders.delete('css')

const vue = require('./loaders/vue')
const pug = require('./loaders/pug')
const sass = require('./loaders/sass')

const path = require('path')
environment.config.merge({
  resolve: {
    alias: {
      '@': path.resolve('app/javascript'),
      vue$: 'vue/dist/vue.runtime.esm.js',
    },
  },

})

environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.plugins.prepend('VuetifyLoaderPlugin', new VuetifyLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('pug', pug)
environment.loaders.prepend('sass', sass)

module.exports = environment
