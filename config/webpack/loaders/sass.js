const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const { config } = require('@rails/webpacker')

module.exports = {
  test: /\.s(c|a)ss$/,
  use: [
    config.extract_css === false
    ? 'vue-style-loader'
    : MiniCssExtractPlugin.loader,
    {
      loader: 'css-loader',
      options: {
        sourceMap: true,
        importLoaders: 2
      }
    },
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true,
        implementation: require('sass'),
        sassOptions: {
          fiber: require('fibers'),
          indentedSyntax: true
        }
      }
    }
  ]
}
