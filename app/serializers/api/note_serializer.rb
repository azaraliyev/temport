class Api::NoteSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :content
end
