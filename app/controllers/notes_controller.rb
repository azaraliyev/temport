class NotesController < ApplicationController
  load_and_authorize_resource

  def index
    q = @notes.ransack(params[:q])

    pagy, records = pagy(q.result)

    render inertia: 'Notes/Index', props: {
      reports: -> {
        Report.
          order(:name).
          as_json(only: [ :id, :name ])
      },
      notes: jbuilder do |json|
        json.data(records, :id, :title, :content)
        json.meta pagy_metadata(pagy)
      end,
      filters: params.slice(:q),
      can: {
        create_note: can?(:create, Note)
      },
    }
  end

  def new
    render inertia: 'Notes/New', props: {
      note: jbuilder do |json|
        json.(@note, :title, :content)
      end
    }
  end

  def create
    if @note.update(note_params)
      redirect_to notes_path, notice: 'Note created.'
    else
      redirect_to new_note_path, errors: @note.errors
    end
  end

  def edit
    render inertia: 'Notes/Edit', props: {
      note: jbuilder do |json|
        json.(@note, :id, :title, :content)
      end
    }
  end

private

  def note_params
    params.fetch(:note, {}).permit(
      :title, :content
    )
  end
end
