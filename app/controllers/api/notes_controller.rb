class Api::NotesController < ActionController::API
  include Auth
  include InertiaJson
  include Pagy::Backend
  
  def index
    q = Note.order(created_at: :desc).ransack(params[:q])

    pagy, records = pagy(q.result)

    options = {}
    options[:meta] = {
      pagination: pagy_metadata(pagy),
      filters: params.slice(:q)
    }
    render json: Api::NoteSerializer.new(records, options).serialized_json
  end
end