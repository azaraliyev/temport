class ReportsController < ApplicationController
  load_and_authorize_resource

  def index
    q = @reports.ransack(params[:q])

    pagy, records = pagy(q.result)

    render inertia: 'Reports/Index', props: {
      reports: jbuilder do |json|
        json.data(records, :id, :name, :deleted_at)
        json.meta pagy_metadata(pagy)
      end,
      filters: params.slice(:q),
      can: {
        create_report: can?(:create, Report)
      },
    }
  end

  def new
    render inertia: 'Reports/New', props: {
      report: jbuilder do |json|
        json.(@report, :name)
      end
    }
  end

  def create
    if @report.update(report_params)
      redirect_to reports_path, notice: 'Report created.'
    else
      redirect_to reports_path, errors: @report.errors
    end
  end


  private

  def report_params
    params.fetch(:report, {}).permit(
      :name, :rank
    )
  end
end
