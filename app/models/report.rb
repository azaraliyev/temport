class Report < ApplicationRecord
  validates :name, presence: true

  include SoftDelete
end
