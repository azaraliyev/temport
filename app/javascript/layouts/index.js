import BlankLayout from './BlankLayout.vue'
import BasicLayout from './BasicLayout.vue'
import MainLayout from './MainLayout.vue'
import CenteredLayout from './CenteredLayout.vue'

export { BasicLayout, BlankLayout, MainLayout, CenteredLayout }
