import routes from '@/utils/routes.js'

export default {
  install (Vue, options) {
    Vue.prototype.$routes = routes
  }
}
