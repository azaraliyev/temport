import Vue from 'vue'
// import Vuetify from 'vuetify/lib'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#0a2b4e'
      }
    }
  }
}

export default new Vuetify(opts)
