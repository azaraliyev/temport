require("@rails/ujs").start()
require("channels")

const images = require.context('../images', true)
const imagePath = (name) => images(`./${name}`, true).default
Vue.prototype.imagePath = imagePath

import Vue from 'vue'
import VueMeta from 'vue-meta'

import VueCompositionApi from '@vue/composition-api'

Vue.use(VueCompositionApi)

import axios from 'axios'
import VueAxios from 'vue-axios'
import store from '../store'
import vuetify from '@/plugins/vuetify'
import router from '@/plugins/router'

import Pagination from '@/shared/pagination'
import Breadcrumb from '@/shared/breadcrumb'
import Search from '@/shared/search'

Vue.component('pagination', Pagination)
Vue.component('search', Search)
Vue.component('breadcrumb', Breadcrumb)

Vue.prototype.imagePath = imagePath

import '../styles/application.sass'

Vue.prototype.$http = axios

axios.defaults.xsrfHeaderName = 'X-CSRF-Token'
Vue.use(VueAxios, axios)

Vue.use(VueMeta)

Vue.use(router)

import PortalVue from 'portal-vue'
Vue.use(PortalVue)

import MatomoTracker from '@/utils/matomo-tracker'
Vue.use(MatomoTracker)

import showFlash from '@/utils/flash'

import { InertiaApp } from '@inertiajs/inertia-vue'
Vue.use(InertiaApp)

const app = document.getElementById('app')

new Vue({
  metaInfo: {
    titleTemplate: (title) => title ? `${title} - CBM Calibration` : 'CBM Calibration',
  },
  store,
  vuetify,
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(`@/pages/${name}`).then(module => module.default),
      transformProps: props => {
        Vue.nextTick(() => {
          showFlash(props)
        })

        if (Vue.matomo.enabled)
          setTimeout(() => {
            Vue.matomo.trackPageView()
          }, 100)

        return props
      },
    },
  }),
}).$mount(app)
