FactoryBot.define do
  factory :report do
    name { "MyString" }
    rank { "" }
    deleted_at { "2020-04-03 15:21:08" }
  end
end
