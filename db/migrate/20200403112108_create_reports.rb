class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :name
      t.integer :rank
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
